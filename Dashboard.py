from matplotlib.pyplot import figure
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import numpy as np
import seaborn as sns
import os
import pandas as pd
from datetime import datetime
from datetime import timedelta
import plotly.graph_objects as go
from jupyter_dash import JupyterDash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import dash
import dash_bootstrap_components as dbc
from pandas import DataFrame
from io import StringIO
import requests
from flask import Flask, request, Response, jsonify
_app_route='/'
from dash.exceptions import PreventUpdate
import argparse
from googleapiclient.discovery import build
import httplib2
from oauth2client import client
from oauth2client import file
from oauth2client import tools
import plotly.express as px

def create_keyfile_dict():
    variables_keys = {
        "type": "service_account",
        "project_id": "thei-314609",
        "private_key_id": "dfbc258097493f83ae5bf7b460e2b12b2c3c8485",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCquHNXGmdYZDZk\nsUdjwPR1UgIMm0GJaqnQGw8Hw1Zr99mQqSQJa6+uSxBCp5AXB73+0DJzGg0Xx2vj\nRvArQeB32Naw3RVXP4PiKCvT9VK/H3gHkhlgCRWxhRH7jsqz/nEuixcIzSQh4fX2\nzalwqv5CHNHf42NigOmmR3MtT6nGFNc2iHazo4CJkdgYq/Mg1ADD7aP44UXvKlq+\nkv+FZjFukshOxtDPkUMYeHH2cws+6YFJuufmSNGDidC2OtrdGfhmNkTFQk6SCOK4\njX0b+K9iXKUexWfc/MuUSObl1iFiS59C9IRBs4PV/6//zrUoUe/QGR1mqjEo5shU\nFC0315cZAgMBAAECggEAB5pANKxN1t9blr9YTQZuPH1C7xnI4Nw5pLVXxYI66oL2\nkt0daDf98laQak94+LWVDfBqlrqdw6XTVaM2lc6QtvWWlfi6dxCyiFM+pGlD3pQB\nreChNagh6a5h3yB8QWi+1JJIG9G63rFn2S5OceCDgyEybXR8XAoenZOH+wgVqOb5\nECNswTf5GMSUgYrbF4rKrcbNfwzJmbC/u1is0hD1WhmAQSt/9daoRQhpJKVuAPWt\nsW6pCdx7kSd9KbcPXNRyZdKVm8VpF5cz9+A0L6RIJZpaHdLWrnhbceCo5DBnqcjL\nPKZwV4N2pZ46MyXVLinbUAL+zgqu7cnZYWjb7BvXkwKBgQDW0CaMGb1DCpLO733h\nPA8VumWXxSht4S0UPD1i4wEfsXqLd86uC4vUqk5cbvfMRwRdVin3iIl+gKDbT2hP\nA4I/5/6bN6wfagYC3TZSvS6G85kDzdGNPuks5M3nQTBa11pDiea5TLdPkdNBUNLp\nE/RcSMIEfc1g5BcF6f0aOIBpNwKBgQDLdBE6Dz5pSgs/iMJMdAUmomwsfptbrMTh\nc5szONj+WSelCByogN00NsF3jBSEXJPQZFacM1WtILqHYzTek3ruBSumtwfrnVmI\ncMtAqevnRQLl8FZFbckRh4Q6pUeJkDWkwGgY8OEsaJY781sHPpkBlI7x32PmM0mr\nleIm9KjqLwKBgQCbHnJHhyq5Z8A9uImBBRqriQ9lulSIjCs+SoXVi7E7DrE8V0aj\nPek6p8SwUN2V4M6RC/CANFN9SytdUwkyzYjX1xJvSZN8l9ZBaTaaJsybXPro2U5y\nHbTJ22uoi9V8M7iVvtYihvh0eAxCRTgYYXV60ntwPNv7FJpfhelkbtqjnwKBgQCQ\nINJPWhsxhZXAVYanmI1JmEifoLU4VYVH3Zj1yXDXhpsyKzP6HwgVhDC8Gs0yE09X\nkq1UjH1GX2gknyAGCV5NnfYVbZY70MVrhdRHI9kNhfMOp5rGJocJVwJSdWHfTvzh\nkLht6r7psdGz+Krkz/CekPpO2J0xp+Wpm02RoULEWwKBgFQoLaG+HvdHd/Tp+7jx\naOcMZSCpN9BE+ESMyRM0VEIZPeNQchcNXPIkX2E5Ij9p3Sc0HDDbUZ0zbWtEEE+T\nxzRxkT9HN2TywNySkz4IvjkggufMJue4nm5moFR6HJ0QE5b5iGMAgs7ZbwhBwQW+\nQywM/7ablYFFkxXLfAN04j+p\n-----END PRIVATE KEY-----\n",
        "client_email": "thei-310@thei-314609.iam.gserviceaccount.com",
        "client_id": "104804534320072376207",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/thei-310%40thei-314609.iam.gserviceaccount.com"
    }
    return variables_keys
create_keyfile_dict()

def update_data():
    scope = ['https://spreadsheets.google.com/feeds']
    credentials = ServiceAccountCredentials.from_json_keyfile_dict(create_keyfile_dict(),scope)
    gc = gspread.authorize(credentials)
    spreadsheet_key = '1f1FIf7gudzgOhSk6pB5llpETmNBaj_OUJMYh8RQ1OvQ'
    book = gc.open_by_key(spreadsheet_key)
    worksheet = book.worksheet("2016_lc_age_sex_en")
    table = worksheet.get_all_values()
    return  table
update_data()




def rem_cols():
    table = update_data()
    df = pd.DataFrame(table[1:], columns=table[0])
    df = df.apply(pd.to_numeric, errors='ignore')
    df.columns=df.columns.str.replace(' ','')
    return df
rem_cols()


def data_pre_processing():
     df = rem_cols()
     df1 = pd.DataFrame(data=None, index=None, columns=None)
     df1['Sno']=df['Sno'].astype('category')
     df1['Constituencies']=df['Constituencies'].astype('category')
     df1['Sex']=df['Sex'].astype('category')
     df1["Count"]=df.Count.astype(str)
     df1["Count"]=df1["Count"].astype(float)
     df1["Count"] =df1["Count"].astype(np.int64)
     df1['AgeRange']=df['AgeRange'].astype('category')     
     return df1
data_pre_processing()




df1 = data_pre_processing()
Counts = df1.Count
Constituency = df1.Constituencies
Sum1=0
Count1=0
Sum2=0
Count2=0
Sum3=0
Count3=0
Sum4=0
Count4=0
Sum5=0
Count5=0


for Counts, Constituency in zip(Counts, Constituency):
    if (Constituency == "HONG KONG ISLAND"):
        Sum1 = Sum1 + Counts
        Count1 = Count1 + 1
    elif (Constituency == "KOWLOON WEST"):
        Sum2 = Sum2 + Counts
        Count2 = Count2 + 1
    elif (Constituency == "KOWLOON EAST"):
        Sum3 =  Sum3 + Counts
        Count3 = Count3 + 1
    elif (Constituency == "NEW TERRITORIES WEST"):
        Sum4 =  Sum4 + Counts
        Count4 = Count4 + 1
    elif (Constituency == "NEW TERRITORIES EAST"):
        Sum5 =  Sum5 + Counts
        Count5 = Count5 + 1    

USources=  ['HONG KONG ISLAND', 'KOWLOON WEST', 'KOWLOON EAST', 'NEW TERRITORIES WEST', 'NEW TERRITORIES EAST']
a = USources
b = [Sum1, Sum2, Sum3, Sum4, Sum5]
votes_by_source_sum = go.Figure([go.Bar(x=a, y=b)])
votes_by_source_sum.update_layout(paper_bgcolor='rgba(0,0,0,0)',plot_bgcolor='rgba(0,0,0,0)', template="plotly_dark")
c = USources
d = [Count1, Count2, Count3, Count4, Count5]  
votes_by_source_count = go.Figure([go.Scatter(x=c, y=d)])
votes_by_source_count.update_layout(paper_bgcolor='rgba(0,0,0,0)',plot_bgcolor='rgba(0,0,0,0)' ,template="plotly_dark")




df1 = data_pre_processing()
Counts = df1.Count
Gender = df1.Sex
Sum1=0
Count1=0
Sum2=0
Count2=0


for Counts, Gender in zip(Counts, Gender):
    if (Gender == "M"):
        Sum1 = Sum1 + Counts
        Count1 = Count1 + 1
    elif (Gender == "F"):
        Sum2 = Sum2 + Counts
        Count2 = Count2 + 1
    
USources=  ['MALE', 'FEMALE']
a = USources
b = [Sum1, Sum2]
votes_by_gender_sum = go.Figure([go.Bar(x=a, y=b)])
votes_by_gender_sum.update_layout(paper_bgcolor='rgba(0,0,0,0)',plot_bgcolor='rgba(0,0,0,0)', template="plotly_dark")
c = USources
d = [Count1, Count2]  
votes_by_gender_count = go.Figure([go.Scatter(x=c, y=d)])
votes_by_gender_count.update_layout(paper_bgcolor='rgba(0,0,0,0)',plot_bgcolor='rgba(0,0,0,0)' ,template="plotly_dark")


df1 = data_pre_processing()
Counts = df1.Count
Age = df1.AgeRange
Sum1=0
Count1=0
Sum2=0
Count2=0
Sum3=0
Count3=0
Sum4=0
Count4=0
Sum5=0
Count5=0
Sum6=0
Count6=0
Sum7=0
Count7=0
Sum8=0
Count8=0
Sum9=0
Count9=0
Sum10=0
Count10=0
Sum11=0
Count11=0
Sum12=0
Count12=0


for Counts, Age in zip(Counts, Age):
    if (Age == "18-20"):
        Sum1 = Sum1 + Counts
        Count1 = Count1 + 1
    elif (Age == "21-25"):
        Sum2 = Sum2 + Counts
        Count2 = Count2 + 1
    elif (Age == "26-30"):
        Sum3 =  Sum3 + Counts
        Count3 = Count3 + 1
    elif (Age == "31-35"):
        Sum4 =  Sum4 + Counts
        Count4 = Count4 + 1
    elif (Age == "36-40"):
        Sum5 =  Sum5 + Counts
        Count5 = Count5 + 1
    elif (Age == "41-45"):
        Sum6 =  Sum6 + Counts
        Count6 = Count6 + 1
    elif (Age == "46-50"):
        Sum7 =  Sum7 + Counts
        Count7 = Count7 + 1
    elif (Age == "51-55"):
        Sum8 =  Sum8 + Counts
        Count8 = Count8 + 1
    elif (Age == "56-60"):
        Sum9 =  Sum9 + Counts
        Count9 = Count9 + 1
    elif (Age == "61-65"):
        Sum10 =  Sum10 + Counts
        Count10 = Count10 + 1
    elif (Age == "66-70"):
        Sum11 =  Sum11 + Counts
        Count11 = Count11 + 1
    elif (Age == "71+"):
        Sum12 =  Sum12 + Counts
        Count12 = Count12 + 1    

USources=  ['18-20', '21-25', '26-30', '31-35', '36-40', '41-45','46-50', '51-55', '56-60', '61-65', '66-70', '71+' ]
a = USources
b = [Sum1, Sum2, Sum3, Sum4, Sum5, Sum6, Sum7, Sum8, Sum9, Sum10, Sum11, Sum12]
votes_by_age_sum = go.Figure([go.Bar(x=a, y=b)])
votes_by_age_sum.update_layout(paper_bgcolor='rgba(0,0,0,0)',plot_bgcolor='rgba(0,0,0,0)', template="plotly_dark")
c = USources
d = [Count1, Count2, Count3, Count4, Count5, Count6, Count7, Count8, Count9, Count10, Count11, Count12]  
votes_by_age_count = go.Figure([go.Scatter(x=c, y=d)])
votes_by_age_count.update_layout(paper_bgcolor='rgba(0,0,0,0)',plot_bgcolor='rgba(0,0,0,0)' ,template="plotly_dark")


app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
server=app.server

app.layout = html.Div([
    
      dbc.Row(className="BrandBar",
      children=[ 
                  dbc.Col([
                      html.P("DATA VISUALIZATION ASSIGNMENT")
                 ])
                  
              ], style={"background":'#fff'},),

     
      dbc.Row(
          [
              
              
              
              
              dbc.Col(  
              [
                  dbc.Row( className = "drp",

                   children=[
                       dbc.Col(html.Div("DATA VISUALIZATION" ,style={"float":"left", 'padding-left':'7vw'})),
                       
                     
                      
                  ], style={"padding-top":"3vh"}),
                  
                  html.Br(),
                  
                  
                  dbc.Row([
                        dbc.Col([ 
                            html.H3("VOTES BY SOURCE SUM"),
                            dcc.Graph(figure=votes_by_source_count),
                            
                
                                ],style={"box-shadow":"7px 7px 7px #000"}, width = 4),
                        dbc.Col([
                            html.H3("VOTES BY SOURCE SUM"),
                            dcc.Graph(figure=votes_by_source_sum),
                            
                
                                ],style={"box-shadow":"7px 7px 7px #000"}, width = 4),
                      dbc.Col([
                            html.H3("VOTES BY GENDER COUNT"),
                            dcc.Graph(figure=votes_by_gender_count),
                            
                
                                ],style={"box-shadow":"7px 7px 7px #000"}, width = 4)
                ], className = 'row'),
                  
                  
                  html.Br(),
                  
                  dbc.Row([
                        dbc.Col([
                            html.H3("VOTES BY GENDER SUM"),
                            dcc.Graph(figure=votes_by_gender_sum),
                            
                
                                ],style={"box-shadow":"7px 7px 7px #000"}, width = 4),
                        dbc.Col([
                            html.H3("VOTES BY AGE COUNT"),
                            dcc.Graph(figure=votes_by_age_count),
                            
                
                                ],style={"box-shadow":"7px 7px 7px #000"}, width = 4),
                       dbc.Col([
                            html.H3("VOTES BY AGE SUM"),
                            dcc.Graph(figure=votes_by_age_sum),
                            
                
                                ],style={"box-shadow":"7px 7px 7px #000"}, width = 4),
                      
                ], className = 'row'),
                  
                  html.Br(),
                  
              ])
          ])
    ])


app.run_server(debug=False)